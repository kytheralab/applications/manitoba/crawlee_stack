import os
from ament_index_python.packages import get_package_prefix
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess,LogInfo
from launch.substitutions import LaunchConfiguration, Command
from launch_ros.actions import Node
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource

import xacro

def generate_launch_description():

    # Declare packages localization
    pkg_crawlee_gazebo = get_package_share_directory('crawlee_gazebo')
    pkg_crawlee_description = get_package_share_directory('crawlee_description')

    # Parse robot description from xacro
    robot_description_file =  os.path.join(pkg_crawlee_description, 'urdf/', 'crawlee.xacro')    
    robot_description_config = xacro.process_file(robot_description_file)
    robot_description = {"robot_description": robot_description_config.toxml()}

    # Launch Arguments
    use_sim_time = LaunchConfiguration('use_sim_time', default=True)

    # Intialize argument from et for roslaunch
    declared_arguments = []
    declared_arguments.append(DeclareLaunchArgument('use_sim_time', default_value=use_sim_time, description='If true, use simulated clock'))
    declared_arguments.append(DeclareLaunchArgument('robot_name', default_value='crawlee',description='Robot Name')) 
    declared_arguments.append(DeclareLaunchArgument('robot_coord_X',default_value='0',description='Robot Spawn Coordinate X')) 
    declared_arguments.append(DeclareLaunchArgument('robot_coord_Y', default_value='0',description='Robot Spawn Coordinate Y'))
    declared_arguments.append(DeclareLaunchArgument('robot_coord_Z', default_value='0',description='Robot Spawn Coordinate Z'))
    declared_arguments.append(DeclareLaunchArgument('robot_orien_R', default_value='0.0',description='Robot Spawn Orientation R')) 
    declared_arguments.append(DeclareLaunchArgument('robot_orien_P', default_value='0',description='Robot Spawn Orientation P'))
    declared_arguments.append(DeclareLaunchArgument('robot_orien_Y', default_value='1.56',description='Robot Spawn Orientation Y'))

    #declared_arguments.append(DeclareLaunchArgument('robot_namespace', default_value='',description='Robot Name Space'))       
    #declared_arguments.append(DeclareLaunchArgument( 'use_sim_time',  default_value='false',  description='Use simulation (Gazebo) clock if true'))

   

    #Initialize MODEL_PATH Global Variables
    # install_dir = get_package_prefix('crawlee_gazebo')
    # if 'IGN_GAZEBO_MODEL_PATH' in os.environ:
    #     os.environ['IGN_GAZEBO_MODEL_PATH'] =  os.environ['IGN_GAZEBO_MODEL_PATH'] + ':' + install_dir + '/share'
    # else:
    #     os.environ['IGN_GAZEBO_MODEL_PATH'] =  install_dir + "/share"

    # if 'IGN_GAZEBO_PLUGIN_PATH' in os.environ:
    #     os.environ['IGN_GAZEBO_PLUGIN_PATH'] = os.environ['IGN_GAZEBO_PLUGIN_PATH'] + ':' + install_dir + '/lib'
    # else:
    #     os.environ['IGN_GAZEBO_PLUGIN_PATH'] = install_dir + '/lib'
 

    # Spawn Robot
    node_spawn_crawlee = Node(package='ros_gz_sim', executable='create',
                arguments=[
                    '-name', LaunchConfiguration('robot_name'),
                    '-topic', '/robot_description',
                    '-x', LaunchConfiguration('robot_coord_X'),
                    '-z', LaunchConfiguration('robot_coord_Z'),
                    '-y', LaunchConfiguration('robot_coord_Y'),
                    '-R', LaunchConfiguration('robot_orien_R'),
                    '-P', LaunchConfiguration('robot_orien_P'),
                    '-Y', LaunchConfiguration('robot_orien_Y'),
                    ],
                output='screen',
                )

    # Robot state publisher
    node_robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='both',
        parameters=[robot_description],
    )

    return LaunchDescription(declared_arguments+ 
        [            
            node_spawn_crawlee,
            node_robot_state_publisher        
        ]
    )