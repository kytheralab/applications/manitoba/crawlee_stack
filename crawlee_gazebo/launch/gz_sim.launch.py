import os
import xacro
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.actions import Node

def generate_launch_description():

    # Package Directories
    pkg_ros_gz_sim = get_package_share_directory('ros_gz_sim')
    pkg_crawlee_gazebo = get_package_share_directory('crawlee_gazebo')
    pkg_crawlee_description = get_package_share_directory('crawlee_description')

    #Declare Launch Arguments    
    declare_world = DeclareLaunchArgument(
          'gz_args', #ign_args
          default_value=[os.path.join(pkg_crawlee_gazebo, 'worlds', 'actor.sdf') +
                         ' -v 2 ' +
                        ' -r ' 
                        # --gui-config ' +
                         #os.path.join(pkg_balancee_gazebo, 'config', 'gui.config')
                        , ''],
          description='Gazebo Sim arguments')

    # Gazebo Sim
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_ros_gz_sim, 'launch', 'gz_sim.launch.py')),           
    )

    # Gazebo Sim - ROS Bridge
    bridge = Node(
        package='ros_gz_bridge',
        executable='parameter_bridge',
        parameters=[{ 
                    'config_file':os.path.join(pkg_crawlee_gazebo, 'config', 'gz_bridge.yaml')
            }],
        remappings=[
            ('/world/empty/model/basic_diff_drive_robot/joint_state', 'joint_states'),
        ],
        output='screen'
    )

    #Spawn Robot
    spawn_crawlee = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_crawlee_gazebo, 'launch', 'spawn_crawlee_gz_sim.launch.py'),
        ),
        launch_arguments={'robot_coord_X':'0','robot_coord_Y':'0','robot_coord_Z':'0','robot_orien_Y':'1.56'}.items()
    )

    return LaunchDescription(
        [   
            declare_world,         
            gazebo,          
            spawn_crawlee,
            bridge,            
        ]
    )
