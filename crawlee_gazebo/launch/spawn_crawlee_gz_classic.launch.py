import os
from ament_index_python.packages import get_package_prefix
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess,LogInfo
from launch.substitutions import LaunchConfiguration, Command
from launch_ros.actions import Node
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource

import xacro

def generate_launch_description():
   
    #Intialize argument from et for roslaunch
    declared_arguments = []
    declared_arguments.append(DeclareLaunchArgument('robot_coord_X',default_value='0',description='Robot Spawn Coordinate X')   ) 
    declared_arguments.append(DeclareLaunchArgument('robot_coord_Y', default_value='0',description='Robot Spawn Coordinate Y'))
    declared_arguments.append(DeclareLaunchArgument('robot_coord_Z', default_value='0.85',description='Robot Spawn Coordinate Z'))
    declared_arguments.append(DeclareLaunchArgument('robot_orien_W', default_value='1',description='Robot Spawn Orientation W')   ) 
    declared_arguments.append(DeclareLaunchArgument('robot_orien_X', default_value='0',description='Robot Spawn Orientation X'))
    declared_arguments.append(DeclareLaunchArgument('robot_orien_Y', default_value='0',description='Robot Spawn Orientation Y'))
    declared_arguments.append(DeclareLaunchArgument('robot_orien_Z', default_value='0',description='Robot Spawn Orientation Z'))
    declared_arguments.append(DeclareLaunchArgument('robot_namespace', default_value='',description='Robot Name Space'))
    declared_arguments.append(DeclareLaunchArgument('robot_name', default_value='crawlee',description='Robot Name'))    
    declared_arguments.append(DeclareLaunchArgument( 'use_sim_time',  default_value='false',  description='Use simulation (Gazebo) clock if true'))
   

    #Load URDF
    xacro_file = os.path.join(get_package_share_directory('crawlee_description'), 'urdf/', 'crawlee_gz_classic.xacro')    
    assert os.path.exists(xacro_file), "The crawlee.xacro doesnt exist in "+str(xacro_file)
    robot_description_config = xacro.process_file(xacro_file)
    robot_desc = robot_description_config.toxml()
    print ("Robot conversion to URDF done.")

    #Initialize MODEL_PATH Global Variables
    install_dir = get_package_prefix('crawlee_gazebo')

    if 'GAZEBO_MODEL_PATH' in os.environ:
        os.environ['GAZEBO_MODEL_PATH'] =  os.environ['GAZEBO_MODEL_PATH'] + ':' + install_dir + '/share'
    else:
        os.environ['GAZEBO_MODEL_PATH'] =  install_dir + "/share"

    if 'GAZEBO_PLUGIN_PATH' in os.environ:
        os.environ['GAZEBO_PLUGIN_PATH'] = os.environ['GAZEBO_PLUGIN_PATH'] + ':' + install_dir + '/lib'
    else:
        os.environ['GAZEBO_PLUGIN_PATH'] = install_dir + '/lib'


    #Configure Nodes
    node_gazebo_spawn = Node(package='crawlee_gazebo', 
            executable='spawn_crawlee',
            arguments=[LaunchConfiguration('robot_name'), robot_desc, LaunchConfiguration('robot_namespace'), 
                        'world', LaunchConfiguration('robot_coord_X'), LaunchConfiguration('robot_coord_Y'), 
                        LaunchConfiguration('robot_coord_Z'), LaunchConfiguration('robot_orien_W'), LaunchConfiguration('robot_orien_X'),
                        LaunchConfiguration('robot_orien_Y'),LaunchConfiguration('robot_orien_Z'),],            
            output='screen'
            )
    
    node_robot_state_pub = Node(
            package="robot_state_publisher",
            executable="robot_state_publisher",
            name="robot_state_publisher",
            parameters=[
                {'robot_description':Command(['xacro',' ', xacro_file])}],
            output="screen"
            )  

    return LaunchDescription(declared_arguments+[        
        node_gazebo_spawn,
        node_robot_state_pub,            
    ])

