#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():

    pkg_crawlee_gazebo = get_package_share_directory('crawlee_gazebo')
    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')


    # Declare Launch arguments
    world = LaunchConfiguration('world')
    headless = LaunchConfiguration('headless')

    declare_world = DeclareLaunchArgument(
          'world',
          default_value=[os.path.join(pkg_crawlee_gazebo, 'worlds', 'crawlee.world'), ''],
          description='SDF world file')
    
    declare_headless_opt = DeclareLaunchArgument(
        name='headless',
        default_value='False',
        description='Whether to execute gzclient')

    # Start Gazebo server
    start_gazebo_server_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(os.path.join(pkg_gazebo_ros, 'launch', 'gzserver.launch.py')),
        launch_arguments={'world': world}.items()
        )
 
    # Start Gazebo client    
    start_gazebo_client_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(os.path.join(pkg_gazebo_ros, 'launch', 'gzclient.launch.py')),
        condition=UnlessCondition(headless)
        )
     
    #Spawn Robot
    spawn_crawlee = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_crawlee_gazebo, 'launch', 'spawn_crawlee_gz_classic.launch.py'),
        ),
        launch_arguments={'robot_coord_X':'0.3','robot_coord_Y':'0','robot_coord_Z':'0.8'}.items()
    )     

    return LaunchDescription([
        declare_world,
        declare_headless_opt,
        start_gazebo_server_cmd,
        start_gazebo_client_cmd,
        spawn_crawlee
    ])