import os
import xacro
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument, RegisterEventHandler, ExecuteProcess
from launch.event_handlers import OnProcessExit
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.substitutions import FindPackageShare
from launch_ros.actions import Node

def generate_launch_description():

    # Package Directories
    pkg_ros_gz_sim = get_package_share_directory('ros_gz_sim')
    pkg_crawlee_bringup = get_package_share_directory('crawlee_bringup')
    pkg_crawlee_gazebo = get_package_share_directory('crawlee_gazebo')
    pkg_crawlee_description = get_package_share_directory('crawlee_description')
    pkg_crawlee_control = get_package_share_directory('crawlee_control')

    # Launch configuration variables specific to simulation
    rviz_config_file = LaunchConfiguration('rviz_config_file')
    use_rviz = LaunchConfiguration('use_rviz')

    #Declare Launch Arguments    
    declare_rviz_config_file_cmd = DeclareLaunchArgument(
        'rviz_config_file',
        default_value=os.path.join(pkg_crawlee_description, 'rviz', 'diffbot.rviz'),
        description='Full path to the RVIZ config file to use')      
    declare_use_rviz_cmd = DeclareLaunchArgument(
        'use_rviz',
        default_value='False',
        description='Whether to start RVIZ')
    declare_world = DeclareLaunchArgument(
          'gz_args', #ign_args
          default_value=[os.path.join(pkg_crawlee_bringup, 'worlds', 'tiny_trr_race.sdf') +
                         ' -v 2 ' +
                        ' -r ' 
                        # --gui-config ' +
                         #os.path.join(pkg_crawlee_bringup, 'config', 'gui.config')
                        , ''],
          description='Gazebo Sim arguments')
       
    # Gazebo Sim
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_crawlee_gazebo, 'launch', 'gz_sim.launch.py')),           
    )    

    #Spawn Robot
    spawn_crawlee = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_crawlee_gazebo, 'launch', 'spawn_crawlee_gz_sim.launch.py'),
        ),
        launch_arguments={'robot_coord_X':'3.9','robot_coord_Y':'-1.65','robot_coord_Z':'0.2','robot_orien_R':'0','robot_orien_Y':'1.56'}.items()
    )     

    #Launch Controllers
    launch_controllers_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_crawlee_control, 'launch', 'crawlee_control.launch.py'),
        ),
        launch_arguments={'use_joint_animation':'False'}.items(),
        # condition=IfCondition(launch_controllers),
    )     

    #Rviz
    rviz_cmd = Node(
        condition=IfCondition(use_rviz),
        package='rviz2',
        executable='rviz2',
        name='rviz2',
        arguments=['-d', rviz_config_file],
        output='screen')

 
    #Static Transforms
    st_tf = Node(
            package='tf2_ros',            
            executable='static_transform_publisher',
            arguments= ["0", "0", "0", "0", "0", "0", "lidar_link", "crawlee/base_link/lidar"]
        )
    st_tf2 = Node(
            package='tf2_ros',            
            executable='static_transform_publisher',
            arguments= ["0", "0", "0", "0", "0", "0", "front_camera_link", "crawlee/base_link/front_rgbd_camera"]
        )
    

    #Twist to TwistStamped Converter (necessary when using ros diff_drive_controller)
    t2ts_cmd = Node(
            package='twist_stamper',
            executable ='twist_stamper',
            remappings=[
                ('cmd_vel_in', 'cmd_vel'),
                ('cmd_vel_out','diff_drive_base_controller/cmd_vel'),
            ],
            # arguments= ['-r', 'cmd_vel_in:=cmd_vel' '-r' 'cmd_vel_out:=diff_drive_base_controller/cmd_vel']
        )
    

    # Create the launch description and populate
    ld = LaunchDescription()


    # Add any conditioned actions
    ld.add_action(declare_world)
    ld.add_action(gazebo)
    ld.add_action(spawn_crawlee)
    ld.add_action(declare_rviz_config_file_cmd)
    ld.add_action(declare_use_rviz_cmd)
    ld.add_action(rviz_cmd)      
    ld.add_action(t2ts_cmd)
    ld.add_action(st_tf)
    ld.add_action(st_tf2)
    ld.add_action(launch_controllers_cmd)  
   
 

    return ld   
