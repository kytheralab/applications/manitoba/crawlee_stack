# Stack "crawlee_stack"

This is the stack for the Kythera crawlee wheeled robot with a URDF and associated controllers as well as a gazebo simulation in ROS2


## Demonstration

![Demo of RViz](media/rviz_gui.gif)
<!-- ![Demo of Gazebo](media/run.gif) -->


## Repository Structure
This project has been organized in stacks for better modularity and reusability

  
```
	├──crawlee_description/ 				#contains the urdf description and and assets for robot representation in sims and ROS
  	│	├──launch/
  	│	│	└──basic.launch.py 		#launch rviz visualization of the crawlee robot
  	│	├──meshes/ 				#contains the stl files for acurate rendering
  	│	└──urdf/				#contains the URDF description files of the crawlee_robot
  	├──crawlee_gazebo/ 				#contains the aditional elements for Gazebo simulation
  	│	├──worlds/ 				#basic Gazebo worlds for demonstrations
	│	│ 	└──crawlee.world			#a basic Gazebo world with a coffee table
  	│	└──launch/
	│		├──spawn_crawlee_gz_sim.launch 		#spawn the crawlee_robot in Gazebo Classic
	│		└──gz_sim.launch		#spawn the crawlee_robot in Gazebo simulation (ex Ignition)
  	├──crawlee_control/ 				#ros controllers for robot operation in the ROS eco-system
	│	├──crawlee_control/			#variouses nodes
  	│	│	└──joint_animation.py		#joints animation publisher to check movment
  	│	├──config/
  	│	│	└──crawlee_control.yaml 		#crawlee controllers descriptions
  	│	└──launch/
  	│		└──crawlee_control.launch 		#launch file to spawn the controllers
  	├──media/  					#miscelaneous material : videos, tutos, etc...
```


## How to use

### Prepare your work environment

* Option A : Deploy on your Linux (Ubuntu 22.04 recommended)

[Install ROS2](https://docs.ros.org/en/rolling/Installation/Ubuntu-Install-Debians.html), in version **Full-Desktop**, install Gazebo and configure your [work environment](https://docs.ros.org/en/rolling/Tutorials/Configuring-ROS2-Environment.html)  
(This code has been tested on ROS2 Rolling Gazebo Sim (ex Ignition) Fortress. 

Make sure you have the necessary dependencies by typing in the terminal :  
	```
	sudo apt install ros-rolling-xacro -y; sudo apt install ros-rolling-joint-state-publisher-gui -y; sudo apt install ros-rolling-ros2-control -y; sudo apt install ros-rolling-ros2-controllers -y; sudo apt install ros-rolling-gazebo-ros2-control -y
	```

Also add in your workspace the package [twist_stamper](https://github.com/joshnewans/twist_stamper)

### Run the stack

1. Clone this repository in you colcon workspace:   
	`colcon_ws/src`

2. Open a terminal in your catkin_ws:  
	`colcon build`
	
3. Launch the demo for RViz:  
	`ros2 launch crawlee_description demo.launch.py use_joint_state_pub:=True use_robot_state_pub:=True`  

Feel free to move the robot using the GUI sliders

4.  Launch the robot in Gazebo Sim(Ex ignition)]   
	- 1. Launch  Gazebo and instantiate the robot + controllers :  
	`ros2 launch crawlee_bringup crawlee_bringup.launch.py use_rviz:=true`

	(There is still a synchronizatio issue on node_controller launch, if the controller_load fails, try restarting the command or rebooting the computer)

	Which is the equivalent of calling : `ros2 launch crawlee_gazebo gz_sim.launch.py` and `ros2 run twist_stamper twist_stamper --ros-args -r cmd_vel_in:=/cmd_vel -r cmd_vel_out:=/diff_drive_base_controller/cmd_vel`  and `ros2 launch crawlee_control crawlee_control.launch.py use_joint_animation:=False` 

	- 2. Drive the robot with 
	    `ros2 run teleop_twist_keyboard teleop_twist_keyboard -r /cmd_vel:=/cmd_vel` 
		or with joystick from [joy_teleop](https://gitlab.com/kytheralab/applications/teleop_joy_cmd_vel) repo :
		`ros2 launch teleop_joy_cmd_vel teleop.launch.py pub_topic:=/diff_drive_base_controller/cmd_vel_unstamped`
	

	
	
## The robot

![Demo of RViz](media/crawlee.png)

## Example in a world
![Demo of Crawlee in TRR](media/crawlee_nose_dive.gif)

## Troubleshoot

If you have error messages, you might need to install aditionnal packages
```
sudo apt install ros-rolling-gazebo-ros
sudo apt install ros-rolling-joint-state-publisher-gui
sudo apt install ros-rolling-joint-state-publisher
sudo apt install ros-rolling-ros2-controllers-test-nodes

```


## ToDo

* Fix Lidar
* Improve simulation realism

	
## Ressources

0. [A guide for ROS2 and Gazebo](https://automaticaddison.com/how-to-simulate-a-robot-using-gazebo-and-ros-2/)
1. [Use XACRO in ROS 2](https://answers.ros.org/question/361623/ros2-robot_state_publisher-xacro-python-launch/)
2. [Tutorial to load URDF in ROS2](https://github.com/olmerg/lesson_urdf)[and the related video](https://www.youtube.com/watch?v=IfpzNFKnkH0)
3. [ROSControl Documentation](https://ros-controls.github.io/control.ros.org/ros2_controllers/doc/controllers_index.html)
4. [A Tutorial on ROS_COntrol in C++](https://jeffzzq.medium.com/designing-a-ros2-robot-7c31a62c535a)


## License

Tbd
