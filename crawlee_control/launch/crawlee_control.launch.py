import os

from launch import LaunchDescription
from launch.substitutions import Command, FindExecutable, PathJoinSubstitution
from ament_index_python.packages import get_package_share_directory

from launch.conditions import IfCondition
from launch_ros.actions import Node
from launch.actions import ExecuteProcess, DeclareLaunchArgument,RegisterEventHandler, IncludeLaunchDescription
from launch_ros.substitutions import FindPackageShare
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch.event_handlers import OnProcessExit
from launch_ros.substitutions import FindPackageShare



def generate_launch_description():

    pkg_crawlee_control = get_package_share_directory('crawlee_control')
    pkg_crawlee_description = get_package_share_directory('crawlee_description')

    # Launch configurations variables 
    robot_controllers = os.path.join(pkg_crawlee_control, 'config/', 'crawlee_controller.yaml')
    robot_description = os.path.join(pkg_crawlee_description, 'urdf/', 'crawlee.xacro')  

    control_node = Node(
        package="controller_manager",
        executable="ros2_control_node",
        parameters=[robot_description, robot_controllers],
        output="both",
    )

    joint_state_broadcaster_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["joint_state_broadcaster", "--controller-manager", "/controller_manager"],
    )

    diff_base_controller_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["diff_drive_base_controller", "--controller-manager", "/controller_manager"],
    )


    # Delay start of robot_controller after `joint_state_broadcaster`
    delay_diff_controller_spawner_after_joint_state_broadcaster_spawner = RegisterEventHandler(
        event_handler=OnProcessExit(
            target_action=joint_state_broadcaster_spawner,
            on_exit=[diff_base_controller_spawner],
        )
    ) 


 

    ld = LaunchDescription()

    ld.add_action(control_node)
    ld.add_action(joint_state_broadcaster_spawner)
    ld.add_action(delay_diff_controller_spawner_after_joint_state_broadcaster_spawner)

    return ld

