import os

from ament_index_python.packages import get_package_share_directory

import launch
import launch_ros.actions


def generate_launch_description():
 

    return launch.LaunchDescription([
        launch.actions.DeclareLaunchArgument('joy_vel', default_value='/diff_drive_base_controller/cmd_vel_unstamped'),
       
       
        
        launch_ros.actions.Node(
            package='teleop_twist_keyboard', 
            executable='teleop_twist_keyboard',
            name='teleop_twist_keyboard_node', 
            remappings={('/cmd_vel', launch.substitutions.LaunchConfiguration('joy_vel'))},
            output='screen'
            ),
    ])